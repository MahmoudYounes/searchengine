Advanced Programming Techniques
Search Engine Project
"ReadMe file"
Semester - Team Number: 8
__________________________________________________________

### how to run the project? (tested on debian) ###
1- import these files from folder "db" on your MySQL database :
	"indexerdb.sql"
	"searchengine.sql"
	
2- Make the "stoppingwords.txt" file from folder "code" visible to your IDE (or your run environment) or put it in the same folder as crawlerIndexer/src/

3- execute run.sh.
