$(document).ready(function (){
    
    var number_of_results = parseInt($('.pages').attr('id'));
	var i;
	
	for(i=1; i<=10 ; i++)
	{
		$('.'+i).addClass('shownR');
	}
	
	var number_of_pages = Math.ceil(number_of_results/10);
	
	for(i=1; i<=number_of_pages; i++)
	{
		$('.pages').append('<div class="pageNumber" id="'+i+'" > '+i+' </div>');
	}
	$('#1').addClass('currentPage');
	
	$(document).delegate('.pageNumber', 'click', function (){
        
		var id = parseInt(this.id);
		
		$('.currentPage').removeClass('currentPage');
		
		$(this).addClass('currentPage');
		
		$('.shownR').each(function() {
		  $( this ).removeClass('shownR');
		});
				
		console.log(id);
		
		var st = ((id-1)*10)+1;
		
		for(i=st; i<=st+9; i++)
		{
			$('.'+i).addClass('shownR');
		}
		
    });

});