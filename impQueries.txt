# count Crawled Pages
SELECT * FROM `WebLinks` WHERE visited = 1 and invalid = 0 and taken = 0

# reset all pages rank to 1
UPDATE WebLinks SET rank = 1 WHERE visited = 1 and taken = 0 and invalid = 0

# reset crawler and indexer databases
DELETE FROM `WebLinks` WHERE 1;
DELETE FROM `linkMap` WHERE 1;
USE indexerdb;
DELETE FROM `keywordlink` WHERE 1;
DELETE FROM `keywordposition` WHERE 1;
USE SearchEngine;
INSERT INTO `WebLinks`(`ID`, `web-urls`, `visited`, `invalid`, `taken`, `web-urls-hashed`, `insertedAt`, `in_links`, `out_links`, `rank`) VALUES (null, "https://www.dmoz.org", 0, 0, 0, SHA1("https://www.dmoz.org"), NOW(), 0, 0, 1) ,(null, "https://www.facebook.com", 0, 0, 0, SHA1("https://www.facebook.com"), NOW(), 0, 0, 1), (null, "https://www.google.com", 0, 0, 0, SHA1("https://www.google.com"), NOW(), 0, 0, 1), (null, "https://www.twitter.com", 0, 0, 0, SHA1("https://www.twitter.com"), NOW(), 0, 0, 1);

