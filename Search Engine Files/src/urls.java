import java.util.*;

public class urls {
	public int urlID;
	public int wordsInFile;
	public Vector<Integer> bodyPos;
	public Vector<Integer> titlePos;
	public Vector<Integer> h1Pos;
	public Vector<Integer> h2Pos;
	public Vector<Integer> h3Pos;
	public Vector<Integer> h4Pos;
	public Vector<Integer> h5Pos;
	public Vector<Integer> h6Pos;
	
	public urls() {
		bodyPos = new Vector<Integer>();
		titlePos = new Vector<Integer>();
		h1Pos = new Vector<Integer>();
		h2Pos = new Vector<Integer>();
		h3Pos = new Vector<Integer>();
		h4Pos = new Vector<Integer>();
		h5Pos = new Vector<Integer>();
		h6Pos = new Vector<Integer>();	
	}
}
