import java.util.*;

public class Ranker 
{
	public Vector<Integer> linksID;
	public Vector<Float> linksPageRanked; // it holds the popularity of each link where it has the same arrangment as linksID
	public Vector<Float> links1Relevance;  // it holds the relevance of each link where it has the same arrangment as linksID
	public Vector<Float> scoreRanked;     // it holds the Ranking of each link where it has the same arrangment as linksID
	public Vector<Vector<urls>> urlsQueryIndexed;
	public Vector<Vector<urls>> urlsQueryIndexedStemmed;
	public Vector<Vector<urls>> urlsQueryIndexedPhrase;
	public Vector<Integer> outputvector;
	public MySqlDatabase db;
	public Ranker()
	{
		linksID = new Vector<Integer>();
		linksPageRanked = new Vector<Float>();
		links1Relevance = new Vector<Float>();
		scoreRanked = new Vector<Float>();
		urlsQueryIndexed = new Vector<Vector<urls>>();
		urlsQueryIndexedStemmed = new Vector<Vector<urls>>();
		urlsQueryIndexedPhrase = new Vector<Vector<urls>>();
		outputvector=new Vector<Integer>();
		db = new MySqlDatabase();
	}
	
	// This function is the one that gets results of the popularity of matching urls to use in the ranking of the resulted pages.
	// It uses the pagerank algorithm .
	public void getPageRank()
	{
		// TODO call yons's function and sends to it "linksID" vector as "this.linksID"
		// note that the output of the pagerank will be normalized from scale 1 to 10 
		for (int i = 0;i < linksID.size();i++)
		{
			this.linksPageRanked.add(Float.parseFloat(db.getPageRank(Integer.toString(linksID.get(i)))));
		}
		//return this.linksPageRanked;
	}
	// this function is the one that calculates the relevance of each link.
	public void calculateRelevance()
	{
		// TODO the real function after taking input from khaled
		// title gives score 100 points.
		// h1,h2,h3,h4,h5,h6 gives scores of 60,50,40,30,20,10 points.
		// body gives score of 5 points.
		Float temp;  
		for(int i =0;i<urlsQueryIndexed.size();i++)
		{
			for(int j =0;j<urlsQueryIndexed.get(i).size();j++)
			{
				if(linksID.indexOf(urlsQueryIndexed.get(i).get(j).urlID)==-1)
				{
					linksID.add(urlsQueryIndexed.get(i).get(j).urlID);
					links1Relevance.add(linksID.indexOf(urlsQueryIndexed.get(i).get(j).urlID) , (float) 0 );
					
				}
				
					temp=links1Relevance.get(linksID.indexOf(urlsQueryIndexed.get(i).get(j).urlID));
					links1Relevance.set(linksID.indexOf(urlsQueryIndexed.get(i).get(j).urlID) , temp+urlsQueryIndexed.get(i).get(j).bodyPos.size()*5 );
					temp=links1Relevance.get(linksID.indexOf(urlsQueryIndexed.get(i).get(j).urlID));
					links1Relevance.set(linksID.indexOf(urlsQueryIndexed.get(i).get(j).urlID) , temp+urlsQueryIndexed.get(i).get(j).h6Pos.size()*10 );
					temp=links1Relevance.get(linksID.indexOf(urlsQueryIndexed.get(i).get(j).urlID));
					links1Relevance.set(linksID.indexOf(urlsQueryIndexed.get(i).get(j).urlID) , temp+urlsQueryIndexed.get(i).get(j).h5Pos.size()*20 );
					temp=links1Relevance.get(linksID.indexOf(urlsQueryIndexed.get(i).get(j).urlID));
					links1Relevance.set(linksID.indexOf(urlsQueryIndexed.get(i).get(j).urlID) , temp+urlsQueryIndexed.get(i).get(j).h4Pos.size()*30 );
					temp=links1Relevance.get(linksID.indexOf(urlsQueryIndexed.get(i).get(j).urlID));
					links1Relevance.set(linksID.indexOf(urlsQueryIndexed.get(i).get(j).urlID) , temp+urlsQueryIndexed.get(i).get(j).h3Pos.size()*40 );
					temp=links1Relevance.get(linksID.indexOf(urlsQueryIndexed.get(i).get(j).urlID));
					links1Relevance.set(linksID.indexOf(urlsQueryIndexed.get(i).get(j).urlID) , temp+urlsQueryIndexed.get(i).get(j).h2Pos.size()*50 );
					temp=links1Relevance.get(linksID.indexOf(urlsQueryIndexed.get(i).get(j).urlID));
					links1Relevance.set(linksID.indexOf(urlsQueryIndexed.get(i).get(j).urlID) , temp+urlsQueryIndexed.get(i).get(j).h1Pos.size()*60 );
					temp=links1Relevance.get(linksID.indexOf(urlsQueryIndexed.get(i).get(j).urlID));
					links1Relevance.set(linksID.indexOf(urlsQueryIndexed.get(i).get(j).urlID) , temp+urlsQueryIndexed.get(i).get(j).titlePos.size()*100 );
				
			}
		}
		for(int i =0;i<urlsQueryIndexedStemmed.size();i++)
		{
			for(int j =0;j<urlsQueryIndexedStemmed.get(i).size();j++)
			{
				if(linksID.indexOf(urlsQueryIndexedStemmed.get(i).get(j).urlID)==-1)
				{
					linksID.add(urlsQueryIndexedStemmed.get(i).get(j).urlID);
					links1Relevance.add(linksID.indexOf(urlsQueryIndexedStemmed.get(i).get(j).urlID) , (float) 0 );
				}
				
					temp=links1Relevance.get(linksID.indexOf(urlsQueryIndexedStemmed.get(i).get(j).urlID));
					links1Relevance.set(linksID.indexOf(urlsQueryIndexedStemmed.get(i).get(j).urlID) ,(float) (temp+0.5*urlsQueryIndexedStemmed.get(i).get(j).bodyPos.size()*5) );
					temp=links1Relevance.get(linksID.indexOf(urlsQueryIndexedStemmed.get(i).get(j).urlID));
					links1Relevance.set(linksID.indexOf(urlsQueryIndexedStemmed.get(i).get(j).urlID) ,(float) (temp+0.5*urlsQueryIndexedStemmed.get(i).get(j).h6Pos.size()*10) );
					temp=links1Relevance.get(linksID.indexOf(urlsQueryIndexedStemmed.get(i).get(j).urlID));
					links1Relevance.set(linksID.indexOf(urlsQueryIndexedStemmed.get(i).get(j).urlID) ,(float) (temp+0.5*urlsQueryIndexedStemmed.get(i).get(j).h5Pos.size()*20) );
					temp=links1Relevance.get(linksID.indexOf(urlsQueryIndexedStemmed.get(i).get(j).urlID));
					links1Relevance.set(linksID.indexOf(urlsQueryIndexedStemmed.get(i).get(j).urlID) ,(float) (temp+0.5*urlsQueryIndexedStemmed.get(i).get(j).h4Pos.size()*30) );
					temp=links1Relevance.get(linksID.indexOf(urlsQueryIndexedStemmed.get(i).get(j).urlID));
					links1Relevance.set(linksID.indexOf(urlsQueryIndexedStemmed.get(i).get(j).urlID) ,(float) (temp+0.5*urlsQueryIndexedStemmed.get(i).get(j).h3Pos.size()*40) );
					temp=links1Relevance.get(linksID.indexOf(urlsQueryIndexedStemmed.get(i).get(j).urlID));
					links1Relevance.set(linksID.indexOf(urlsQueryIndexedStemmed.get(i).get(j).urlID) ,(float) (temp+0.5*urlsQueryIndexedStemmed.get(i).get(j).h2Pos.size()*50) );
					temp=links1Relevance.get(linksID.indexOf(urlsQueryIndexedStemmed.get(i).get(j).urlID));
					links1Relevance.set(linksID.indexOf(urlsQueryIndexedStemmed.get(i).get(j).urlID) ,(float) (temp+0.5*urlsQueryIndexedStemmed.get(i).get(j).h1Pos.size()*60) );
					temp=links1Relevance.get(linksID.indexOf(urlsQueryIndexedStemmed.get(i).get(j).urlID));
					links1Relevance.set(linksID.indexOf(urlsQueryIndexedStemmed.get(i).get(j).urlID) ,(float) (temp+0.5*urlsQueryIndexedStemmed.get(i).get(j).titlePos.size()*100) );
				
			}
		}
		for(int i =0;i<urlsQueryIndexedPhrase.size();i++)
		{
			for(int j =0;j<urlsQueryIndexedPhrase.get(i).size();j++)
			{
				if(linksID.indexOf(urlsQueryIndexedPhrase.get(i).get(j).urlID)==-1)
				{
					linksID.add(urlsQueryIndexedPhrase.get(i).get(j).urlID);
					links1Relevance.add(linksID.indexOf(urlsQueryIndexedPhrase.get(i).get(j).urlID) , (float) 0 );
				}
					temp=links1Relevance.get(linksID.indexOf(urlsQueryIndexedPhrase.get(i).get(j).urlID));
					links1Relevance.set(linksID.indexOf(urlsQueryIndexedPhrase.get(i).get(j).urlID) , temp+3*urlsQueryIndexedPhrase.get(i).get(j).bodyPos.size()*5 );	
					temp=links1Relevance.get(linksID.indexOf(urlsQueryIndexedPhrase.get(i).get(j).urlID));
					links1Relevance.set(linksID.indexOf(urlsQueryIndexedPhrase.get(i).get(j).urlID) , temp+3*urlsQueryIndexedPhrase.get(i).get(j).h6Pos.size()*10 );
					temp=links1Relevance.get(linksID.indexOf(urlsQueryIndexedPhrase.get(i).get(j).urlID));
					links1Relevance.set(linksID.indexOf(urlsQueryIndexedPhrase.get(i).get(j).urlID) , temp+3*urlsQueryIndexedPhrase.get(i).get(j).h5Pos.size()*20 );
					temp=links1Relevance.get(linksID.indexOf(urlsQueryIndexedPhrase.get(i).get(j).urlID));
					links1Relevance.set(linksID.indexOf(urlsQueryIndexedPhrase.get(i).get(j).urlID) , temp+3*urlsQueryIndexedPhrase.get(i).get(j).h4Pos.size()*30 );
					temp=links1Relevance.get(linksID.indexOf(urlsQueryIndexedPhrase.get(i).get(j).urlID));
					links1Relevance.set(linksID.indexOf(urlsQueryIndexedPhrase.get(i).get(j).urlID) , temp+3*urlsQueryIndexedPhrase.get(i).get(j).h3Pos.size()*40 );
					temp=links1Relevance.get(linksID.indexOf(urlsQueryIndexedPhrase.get(i).get(j).urlID));
					links1Relevance.set(linksID.indexOf(urlsQueryIndexedPhrase.get(i).get(j).urlID) , temp+3*urlsQueryIndexedPhrase.get(i).get(j).h2Pos.size()*50 );
					temp=links1Relevance.get(linksID.indexOf(urlsQueryIndexedPhrase.get(i).get(j).urlID));
					links1Relevance.set(linksID.indexOf(urlsQueryIndexedPhrase.get(i).get(j).urlID) , temp+3*urlsQueryIndexedPhrase.get(i).get(j).h1Pos.size()*60 );
					temp=links1Relevance.get(linksID.indexOf(urlsQueryIndexedPhrase.get(i).get(j).urlID));
					links1Relevance.set(linksID.indexOf(urlsQueryIndexedPhrase.get(i).get(j).urlID) , temp+3*urlsQueryIndexedPhrase.get(i).get(j).titlePos.size()*100 );
				
			}
		}
	}
	//this function is the one that combines the 2 weights from "getPageRank" and "calculateRelevance"
	public Vector<Integer> combinedFinalRanking(Vector<Vector<urls>> indexed,Vector<Vector<urls>> indexedStemmed,Vector<Vector<urls>> indexedPhrase)
	{
		this.urlsQueryIndexed=indexed;
		this.urlsQueryIndexedStemmed=indexedStemmed;
		this.urlsQueryIndexedPhrase=indexedPhrase;
		//this.getPageRank();			//edited
		this.calculateRelevance();
		this.getPageRank();			//added
		float maxRank;
		Integer maxid;
		for(int i =0; i<linksID.size() ; i++)
		{
			scoreRanked.add(i,linksPageRanked.get(i)*links1Relevance.get(i));
		}
		for(int i =0;i<scoreRanked.size();i++)
		{
			maxRank=0;
			maxid=0;
			for(int j =0;j<scoreRanked.size();j++)
			{
				if(scoreRanked.get(j)>maxRank)
					maxid=j;
			}
			scoreRanked.set(maxid,(float) -1);
			outputvector.add(i, linksID.get(maxid));
		}
		return this.outputvector;
	}
}