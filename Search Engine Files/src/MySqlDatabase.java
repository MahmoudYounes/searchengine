/**
 * Database Interface file for crawler and ranker.
 */

import java.sql.*;
import java.util.*;

public class MySqlDatabase
{
	// JDBC driver name and database URL
	//private static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
	private static final String DB_URL = "jdbc:mysql://localhost/SearchEngine";

	//  Database credentials
	/*
		note: please create a MySql database user for Search engine database and grant this user full privilages over this database
	*/
	private static final String USER = "root";
	private static final String PASS = "";

	private Connection conn = null;

	public MySqlDatabase()
	{
		try
		{
			//Register JDBC driver
			Class.forName("com.mysql.jdbc.Driver");

			//Open a connection
			System.out.println("Connecting to crawler database...");
			conn = DriverManager.getConnection(DB_URL,USER,PASS);

		}
		catch (Exception e)
		{
			System.out.println("failed to register Database.");
			System.out.println(e.getMessage());
		}
	}

	/**
	 * sets the invalid column of a certain url to be 1 to mark that it is in valid url
	 */
	public void markInvalidURL(String url)
	{
		String sql  = "UPDATE WebLinks ";
		sql        += "SET invalid = 1 ";
		sql 	   += "WHERE `web-urls` = '" + url + "';";

		updateQuery(sql);
	}

    /**
     * gets 50 urls ready to be crawled by little spiders.
     */
	public Queue<String> getUrls()
	{
		try
		{
			String sql 		= "SELECT DISTINCT `web-urls` ";
			sql 		   += "FROM WebLinks ";
			sql 		   += "WHERE visited = 0 and invalid = 0 and taken = 0 ";
			sql 		   += "LIMIT 50;";

			ResultSet rs = query(sql);
			if(rs == null)
				System.out.println("null detected.");

			Queue<String> urls = new LinkedList<String>();
			while(rs.next())
			{
				String test = rs.getString("web-urls");
				urls.add(test);
			}
			return urls;
		}
		catch (Exception e)
		{
			return null;
		}
	}

	/**
	 * inserts url in the linkMap table which preservers the direction of surfing between pages.
	 * so page (src) points to page (dst).
	 */
	public void insertTrackedPrey(String src, String dst) throws Exception
	{
		String sql = "INSERT INTO linkMap (" +
					 "`srcID`, `dstID` " +
					 ") VALUES ('" + src + "','" + dst + "');";
		updateQuery(sql);

	}

	/**
	 * function that retrieves the id of the url from database.
	 */
	public String getUrlID(String url)
	{
		try
		{
			String ret = null;
			String sql 		= "SELECT ID " +
			        	      "FROM WebLinks " +
			        	      "WHERE `web-urls-hashed` = SHA1('" + url + "') " + 
			        	      "LIMIT 1;";
	        
	        ResultSet rs = query(sql);
	        while(rs.next())
	        {
    			ret = rs.getString("ID");
    		}


	        return ret;
		}
		catch (Exception e)
		{
			System.out.println("getUrlData: failed to get url: " + url);
			return null;
		}
	}

	/**
	 * function to execute update, delete and insert queries.
	 */
	public void updateQuery(String sql)
	{
		try
		{
			Statement stmt = conn.createStatement();
			stmt = conn.createStatement();
			int rowsUpdated = stmt.executeUpdate(sql);
			/*
				should be un commented and tested for verbose output
				System.out.println(rowsUpdated + " rows count updated");
			*/
		}
		catch(Exception e)
		{

		}
	}

	/**
	 * function to execute select queries
	 */
	public ResultSet query(String sql)
	{

		try
		{
			Statement stmt = conn.createStatement();
			stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			return rs;
		}
		catch(Exception e)
		{
			System.out.println("executing query failed");
			System.out.println(e.getMessage());
			return null;
		}
	}

	/**
	 * retrieves the connection.
	 */
	public Connection getConnection()
	{
		return conn;
	}

	/**
	 * checks if the results are not equal to null.
	 */
	public boolean checkResults(ResultSet rs)
	{
		if (rs == null)
			return false;
		return true;
	}

	/**
	 * terminates and close the connection with the database.
	 */
	public boolean closeConnection()
	{
		try
		{
			conn.close();
			return true;
		}
		catch(Exception e)
		{
			return false;
		}
	}

	/**
	 * function that calls the stored procedure (incrementOutLink) on a certain
	 * given as input.
	 */
	public void incrementOutLinks(String url)
	{
		try
		{
			String sql = "UPDATE WebLinks " +
			   			 "SET out_links = out_links + 1 "+
			   			 "WHERE `web-urls-hashed` = SHA1('" + url + "')";

			updateQuery(sql);

		}
		catch (Exception e)
		{
			System.out.println("incrementOutLinks: error");
			System.out.println(e.getMessage());
		}
	}

	/**
     * function to get all urls inorder to rank them.
     */
	public List<String> getAllUrlsToBeRanked()
	{
		try
		{
			String  updateSql      = "UPDATE WebLinks " +
							         "SET rank = 1" +
							  		 "WHERE visited = 1 and invalid = 0 and taken = 0; ",
			  		sql            = "SELECT DISTINCT * " +
							  		 "FROM WebLinks " +
							  		 "WHERE visited = 1 and invalid = 0 and taken = 0;";

			updateQuery(updateSql);
			ResultSet rs = query(sql);
			if (rs == null)
			{
				System.out.println("returning null.");
				return null;
			}
			List<String> urls = new ArrayList<String>();
			while(rs.next())
			{
				urls.add(rs.getString("ID"));
			}
			return urls;
		}
		catch (Exception e)
		{
			return null;
		}
	}

	/**
     * function to get all urls map.
     */
	public List<List<String>> getAllUrlsMap()
	{
		try
		{
			String sql 		= "SELECT DISTINCT * ";
			sql 		   += "FROM linkMap ";

			ResultSet rs = query(sql);
			if (rs == null)
			{
				System.out.println("returning null.");
				return null;
			}
			List<List<String>> urls = new ArrayList<List<String>>();
			while(rs.next())
			{
				List<String> urlData = new ArrayList<String>();
				urlData.add(rs.getString("srcID"));
				urlData.add(rs.getString("dstID"));
				urls.add(urlData);
			}
			return urls;
		}
		catch (Exception e)
		{
			return null;
		}
	}

	/**
	 * function that gets a rank of page of id _id as input
	 */
	public String getPageRank(String _id)
	{
		String sql = "SELECT rank " +
		  			 "FROM WebLinks " +
					 "WHERE ID = '" + _id +"' " +
					 "LIMIT 1;";

		try
		{
			ResultSet rs = query(sql);

			String rank = null;
			while(rs.next())
			{
				rank = rs.getString("rank");
			}
			return rank;
		}
		catch(Exception e)
		{
			System.out.println("getPageRank: error");
			System.out.println(e.getMessage());
		}
		return null;

	}

	/**
	 * function that updates page rank.
	 */
	public void updatePageRank(String _id, String rank)
	{
		String sql = "UPDATE WebLinks " +
					 "SET rank = '" + rank + "' " +
					 "WHERE ID = '" + _id +"';";
		
		updateQuery(sql);
	}

	/**
	 * this function takes as input a destination id of page and gets all pages refering to it.
	 */
	public List<String> getPointingPages(String _id)
	{
		String sql = "SELECT srcID " +
				     "FROM linkMap " +
  					 "WHERE dstID = '" + _id + "';";

  		try
  		{
  			ResultSet rs = query(sql);
  			List<String> srcPageIds = new ArrayList<String>();
  			while(rs.next())
  			{
  				srcPageIds.add(rs.getString("srcID"));
  			}
  			return srcPageIds;
  		}
  		catch(Exception e)
  		{

  		}
  		return null;
	}

	/**
	 * function the gets the count of out_links of a page given its id
	 */
	public double getPageOutLinksCount(String _id)
	{
		String sql = "SELECT out_links " +
		 			 "FROM WebLinks " +
		 			 "WHERE ID = '" + _id + "' " + 
		 			 "LIMIT 1;";

		try
		{
			ResultSet rs = query(sql);
			double outlinkCount = 0;
			while(rs.next())
			{
				outlinkCount = Double.parseDouble(rs.getString("out_links"));
			}
			return outlinkCount;
		}
		catch(Exception e)
		{
			System.out.println("getPageOutLinksCount: error");
			System.out.println(e.getMessage());
		}
		return -1;

	}
}
