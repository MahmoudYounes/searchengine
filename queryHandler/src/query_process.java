import java.io.File;
import java.io.IOException;
import java.sql.*;
import org.jsoup.nodes.Document;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import java.util.*;

public class query_process {

    // JDBC driver name and database URL
    private static final String DB_URL = "jdbc:mysql://localhost/indexerdb";
    private static final String USER = "root";
    private static final String PASS = "";
    
    // Crawler's database data
    private static final String CDB_URL = "jdbc:mysql://localhost/SearchEngine";
    private static final String CUSER = "root";
    private static final String CPASS = "";

    Connection conn = null; 
    Statement stmt = null;
    Connection conn2 = null; 
    Statement stmt2 = null;
    Statement stmt3 = null;
    public displayLink linkData(int link_id){
    	displayLink returnVal = new displayLink();
    	Statement state = null;
    	try {
			state = conn2.createStatement();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
    	String sql = "SELECT `web-urls` from WebLinks where ID = " + Integer.toString(link_id);
    	ResultSet rs;
		try {
			rs = state.executeQuery(sql);
			while(rs.next()){
	    		returnVal.link = rs.getString("web-urls");
	    	}
			/*
	    	Document doc;
	    	try {
	    		
				doc = Jsoup.connect(returnVal.link).userAgent("Chrome").get();
				Element body = doc.body();
				Element head = doc.head();
				String txt = body.text();
				String headtxt = head.text();
				returnVal.title = headtxt;
				returnVal.body = txt;
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			*/
			
			sql = "SELECT `title`, `body` FROM `linkresult` WHERE `url`=\"" + returnVal.link+"\"";
			
			//System.out.println(sql);
			
			rs = stmt3.executeQuery(sql);
			while(rs.next()){
	    		returnVal.title = rs.getString("title");
	    		returnVal.body = rs.getString("body");
	    	}
 
 
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return returnVal;
    }
    
    public boolean binarysearch (String word, List<String> arr){
		int l = 0, r = arr.size()-1;

		while(l <= r){
			int mid = (r-l) /2 + l;
			if(arr.get(mid).compareToIgnoreCase(word) > 0)
				r = mid-1;
			else if (arr.get(mid).compareToIgnoreCase(word) < 0)
				l = mid+1;
			else
				return true;
		}
		
		return false;
	}
	
	public query_process() {
		try
        {           
            //Register JDBC driver          
            Class.forName("com.mysql.jdbc.Driver");

            //Open a connection
            conn = DriverManager.getConnection(DB_URL,USER,PASS);
            conn2 = DriverManager.getConnection(CDB_URL,CUSER,CPASS);
            System.out.println("INDEXER: Database connected!");
			stmt = conn.createStatement();
			stmt2 = conn2.createStatement();
			stmt3 = conn.createStatement();
        }
        catch (Exception e)
        {
            System.out.println("INDEXER: Error connecting to DB: "+e.getMessage());
        }

	}
	public int getUrlID(String url)
	{
		try
		{
			int ret = -1;
			String sql 		= "SELECT ID " +
			        	      "FROM WebLinks " +
			        	      "WHERE `web-urls` = '" + url + "'";
	        
	        ResultSet rs = stmt2.executeQuery(sql);

	        int check = 0;
	        while(rs.next())
	        {
	        	if(check == 0)
	        	{
	        		ret = rs.getInt("ID");
	        	}
	        	check++;
	        }
	      /*  if (check >= 1)
	        {
	        	System.out.println("getUrlID: check failed");
	        } */
	        
	        return ret;
		}
		catch (Exception e)
		{
			System.out.println("getUrlData: failed to get url.");
			return -1;
		}
	}
	public Vector<displayLink> q_process(String query, boolean type){
		ResultSet rslink;
		Stemmer stem = new Stemmer();
		query.trim();
		
		Vector<Vector<urls>> positions = new Vector<Vector<urls>> ();
		Vector<Vector<urls>> stemmedVec = new Vector<Vector<urls>> ();
		Vector<Vector<urls>> returnVec = new Vector<Vector<urls>> ();
		String[] words = query.replaceAll("[^a-zA-Z ]", "").trim().toLowerCase().split("\\s+");
		if (type){
	    	//phrase searching
					
			try {
				Vector<Vector<Integer>> linksID = new Vector<Vector<Integer>>();
				for (int i=0; i<words.length; i++){
					
					Vector <Integer> v = new Vector<Integer>();
					urls pos = new urls();
					Vector <urls> wp = new Vector<urls>(); 
					String sql = "SELECT url, position, tag, l.kLid from keywordlink l, keywordposition p  where keyword = '" + words[i] + "' and stemmed = 0 and l.kLid = p.kLid";
					rslink = stmt.executeQuery(sql);
					
					int prev = -1;
					
					while (rslink.next()){
						if(rslink.getInt("kLid") != prev){
							if(pos.bodyPos.size() !=0 || pos.titlePos.size() !=0 ||pos.h2Pos.size() !=0||pos.h1Pos.size() !=0||pos.h3Pos.size() !=0||pos.h4Pos.size() !=0||pos.h5Pos.size() !=0||pos.h6Pos.size() !=0)
							{
								wp.addElement(pos);
								pos = new urls();
							}
							prev = rslink.getInt("kLid");
							pos.urlID = getUrlID(rslink.getString("url"));
							String wordsQuery = "SELECT wordsCount from wordsinlink where url = '"+ rslink.getString("url") +"'";
							ResultSet result = stmt3.executeQuery(wordsQuery);
							while(result.next())
								pos.wordsInFile = result.getInt("wordsCount");
							v.addElement(getUrlID(rslink.getString("url")));
							
						}
						
						if (rslink.getString("tag").equals("Body"))
							pos.bodyPos.addElement(rslink.getInt("position")); 
						else if (rslink.getString("tag").equals("Title"))
							pos.titlePos.addElement(rslink.getInt("position"));
						else if (rslink.getString("tag").equals("h1"))
							pos.h1Pos.addElement(rslink.getInt("position"));
						else if (rslink.getString("tag").equals("h2"))
							pos.h2Pos.addElement(rslink.getInt("position"));
						else if (rslink.getString("tag").equals("h3"))
							pos.h3Pos.addElement(rslink.getInt("position"));
						else if (rslink.getString("tag").equals("h4"))
							pos.h4Pos.addElement(rslink.getInt("position"));
						else if (rslink.getString("tag").equals("h5"))
							pos.h5Pos.addElement(rslink.getInt("position"));
						else if (rslink.getString("tag").equals("h6"))
							pos.h6Pos.addElement(rslink.getInt("position"));
						
					}
					wp.addElement(pos);
					linksID.addElement(v);
					positions.addElement(wp);
				}
				
				Vector<urls> vu = new Vector<urls>();
				for(int j=0; j<linksID.elementAt(0).size(); j++){
					Vector<Integer> idx = new Vector<Integer>();
					idx.addElement(j);
					boolean flag = false;
					for(int i=1; i<linksID.size(); i++){
						int x = linksID.elementAt(i).indexOf(linksID.elementAt(0).elementAt(j));
						
						if (x == -1){
							flag = true;
							break;
						}
						idx.addElement(x);
					}
					if (!flag){
						urls U = new urls();
						for(int q = 0; q< positions.elementAt(0).elementAt(idx.elementAt(0)).bodyPos.size(); q++){	
							boolean check = false;
							for(int w = 1; w<positions.size(); w++){
								
								if(Collections.binarySearch(positions.elementAt(w).elementAt(idx.elementAt(w)).bodyPos, positions.elementAt(0).elementAt(idx.elementAt(0)).bodyPos.elementAt(q)+1) < 0)
								{
									check = true;
									break;
								}
							}
							if (!check)
							{
								U.urlID = positions.elementAt(0).elementAt(idx.elementAt(0)).urlID;
								U.wordsInFile = positions.elementAt(0).elementAt(idx.elementAt(0)).wordsInFile;
								U.bodyPos.addElement(positions.elementAt(0).elementAt(idx.elementAt(0)).bodyPos.elementAt(q));
							}
						}
						for(int q = 0; q< positions.elementAt(0).elementAt(idx.elementAt(0)).titlePos.size(); q++){	
							boolean check = false;
							for(int w = 1; w<positions.size(); w++){
								
								if(Collections.binarySearch(positions.elementAt(w).elementAt(idx.elementAt(w)).titlePos, positions.elementAt(0).elementAt(idx.elementAt(0)).titlePos.elementAt(q)+1) < 0)
								{
									check = true;
									break;
								}
							}
							if (!check)
							{
								U.urlID = positions.elementAt(0).elementAt(idx.elementAt(0)).urlID;
								U.wordsInFile = positions.elementAt(0).elementAt(idx.elementAt(0)).wordsInFile;
								U.titlePos.addElement(positions.elementAt(0).elementAt(idx.elementAt(0)).titlePos.elementAt(q));
							}
						}
						for(int q = 0; q< positions.elementAt(0).elementAt(idx.elementAt(0)).h1Pos.size(); q++){	
							boolean check = false;
							for(int w = 1; w<positions.size(); w++){
								if(Collections.binarySearch(positions.elementAt(w).elementAt(idx.elementAt(w)).h1Pos, positions.elementAt(0).elementAt(idx.elementAt(0)).h1Pos.elementAt(q)+1) < 0)
								{
									check = true;
									break;
								}
							}
							if (!check)
							{
								U.urlID = positions.elementAt(0).elementAt(idx.elementAt(0)).urlID;
								U.wordsInFile = positions.elementAt(0).elementAt(idx.elementAt(0)).wordsInFile;
								U.h1Pos.addElement(positions.elementAt(0).elementAt(idx.elementAt(0)).h1Pos.elementAt(q));
							}
						}
						for(int q = 0; q< positions.elementAt(0).elementAt(idx.elementAt(0)).h2Pos.size(); q++){	
							boolean check = false;
							for(int w = 1; w<positions.size(); w++){
								System.out.println(positions.elementAt(w).elementAt(idx.elementAt(w)).h2Pos);
								System.out.println(positions.elementAt(0).elementAt(idx.elementAt(0)).h2Pos.elementAt(q)+1);
								System.out.println(Collections.binarySearch(positions.elementAt(w).elementAt(idx.elementAt(w)).h2Pos, positions.elementAt(0).elementAt(idx.elementAt(0)).h2Pos.elementAt(q)+1));
								if(Collections.binarySearch(positions.elementAt(w).elementAt(idx.elementAt(w)).h2Pos, positions.elementAt(0).elementAt(idx.elementAt(0)).h2Pos.elementAt(q)+1) < 0)
								{
									check = true;
									break;
								}
							}
							if (!check)
							{
								U.urlID = positions.elementAt(0).elementAt(idx.elementAt(0)).urlID;
								U.wordsInFile = positions.elementAt(0).elementAt(idx.elementAt(0)).wordsInFile;
								U.h2Pos.addElement(positions.elementAt(0).elementAt(idx.elementAt(0)).h2Pos.elementAt(q));
							}
						}
						for(int q = 0; q< positions.elementAt(0).elementAt(idx.elementAt(0)).h3Pos.size(); q++){	
							boolean check = false;
							for(int w = 1; w<positions.size(); w++){
								
								if(Collections.binarySearch(positions.elementAt(w).elementAt(idx.elementAt(w)).h3Pos, positions.elementAt(0).elementAt(idx.elementAt(0)).h3Pos.elementAt(q)+1) < 0)
								{
									check = true;
									break;
								}
							}
							if (!check)
							{
								U.urlID = positions.elementAt(0).elementAt(idx.elementAt(0)).urlID;
								U.wordsInFile = positions.elementAt(0).elementAt(idx.elementAt(0)).wordsInFile;
								U.h3Pos.addElement(positions.elementAt(0).elementAt(idx.elementAt(0)).h3Pos.elementAt(q));
							}
						}
						for(int q = 0; q< positions.elementAt(0).elementAt(idx.elementAt(0)).h4Pos.size(); q++){	
							boolean check = false;
							for(int w = 1; w<positions.size(); w++){
								
								if(Collections.binarySearch(positions.elementAt(w).elementAt(idx.elementAt(w)).h4Pos, positions.elementAt(0).elementAt(idx.elementAt(0)).h4Pos.elementAt(q)+1) < 0)
								{
									check = true;
									break;
								}
							}
							if (!check)
							{
								U.urlID = positions.elementAt(0).elementAt(idx.elementAt(0)).urlID;
								U.wordsInFile = positions.elementAt(0).elementAt(idx.elementAt(0)).wordsInFile;
								U.h4Pos.addElement(positions.elementAt(0).elementAt(idx.elementAt(0)).h4Pos.elementAt(q));
							}
						}
						for(int q = 0; q< positions.elementAt(0).elementAt(idx.elementAt(0)).h5Pos.size(); q++){	
							boolean check = false;
							for(int w = 1; w<positions.size(); w++){
								
								if(Collections.binarySearch(positions.elementAt(w).elementAt(idx.elementAt(w)).h5Pos, positions.elementAt(0).elementAt(idx.elementAt(0)).h5Pos.elementAt(q)+1) < 0)
								{
									check = true;
									break;
								}
							}
							if (!check)
							{
								U.urlID = positions.elementAt(0).elementAt(idx.elementAt(0)).urlID;
								U.wordsInFile = positions.elementAt(0).elementAt(idx.elementAt(0)).wordsInFile;
								U.h5Pos.addElement(positions.elementAt(0).elementAt(idx.elementAt(0)).h5Pos.elementAt(q));
							}
						}
						for(int q = 0; q< positions.elementAt(0).elementAt(idx.elementAt(0)).h6Pos.size(); q++){	
							boolean check = false;
							for(int w = 1; w<positions.size(); w++){
								
								if(Collections.binarySearch(positions.elementAt(w).elementAt(idx.elementAt(w)).h6Pos, positions.elementAt(0).elementAt(idx.elementAt(0)).h6Pos.elementAt(q)+1) < 0)
								{
									check = true;
									break;
								}
							}
							if (!check)
							{
								U.urlID = positions.elementAt(0).elementAt(idx.elementAt(0)).urlID;
								U.wordsInFile = positions.elementAt(0).elementAt(idx.elementAt(0)).wordsInFile;
								U.h6Pos.addElement(positions.elementAt(0).elementAt(idx.elementAt(0)).h6Pos.elementAt(q));	
							}
						}
						if(U.bodyPos.size() !=0 || U.titlePos.size() !=0 ||U.h2Pos.size() !=0||U.h1Pos.size() !=0||U.h3Pos.size() !=0||U.h4Pos.size() !=0||U.h5Pos.size() !=0||U.h6Pos.size() !=0){
							vu.addElement(U);
						}
					}
				}
				returnVec.addElement(vu);
				System.out.println(returnVec);
			}catch (SQLException e) {
				System.out.println("error executing query");
			 }
	    }
		else
		{
			List<String> arr = new ArrayList<String>();
			Scanner scan = null;
			try {
				scan = new Scanner (new File("stoppingwords.txt")); 
			}
			catch(Exception e){
				System.out.println("INDEXER: Couldn't find the 'stoppingwords.txt' file.");
			}
			while (scan != null && scan.hasNext()) //edited
			{
				arr.add(scan.next());
			}
			
			if(scan != null)		//added
				scan.close();
			
			
			try {
				Vector<Vector<Integer>> linksID = new Vector<Vector<Integer>>();
				for (int i=0; i<words.length; i++){
					
					if(!binarysearch(words[i], arr)){
						Vector <Integer> v = new Vector<Integer>();
						urls pos = new urls();
						Vector <urls> wp = new Vector<urls>(); 
						String sql = "SELECT url, position, tag, l.kLid from keywordlink l, keywordposition p  where keyword = '" + words[i] + "' and stemmed = 0 and l.kLid = p.kLid";
						rslink = stmt.executeQuery(sql);
						
						int prev = -1;
						int kaka=0; 
						while (rslink.next()){
						
							if(rslink.getInt("kLid") != prev){
							
								if(pos.bodyPos.size() !=0 || pos.titlePos.size() !=0 ||pos.h2Pos.size() !=0||pos.h1Pos.size() !=0||pos.h3Pos.size() !=0||pos.h4Pos.size() !=0||pos.h5Pos.size() !=0||pos.h6Pos.size() !=0)
								{
									kaka++;
									if (kaka == 76)
										System.out.println("aa");
									wp.addElement(pos);
									pos = new urls();
								}
								
								prev = rslink.getInt("kLid");
								
								pos.urlID = getUrlID(rslink.getString("url"));
								
								String wordsQuery = "SELECT wordsCount from wordsinlink where url = '"+ rslink.getString("url") +"'";
								ResultSet result = stmt3.executeQuery(wordsQuery);
								while(result.next())
									pos.wordsInFile = result.getInt("wordsCount");
								v.addElement(getUrlID(rslink.getString("url")));
								
							}
							
							if (rslink.getString("tag").equals("Body"))
								pos.bodyPos.addElement(rslink.getInt("position")); 
							else if (rslink.getString("tag").equals("Title"))
								pos.titlePos.addElement(rslink.getInt("position"));
							else if (rslink.getString("tag").equals("h1"))
								pos.h1Pos.addElement(rslink.getInt("position"));
							else if (rslink.getString("tag").equals("h2"))
								pos.h2Pos.addElement(rslink.getInt("position"));
							else if (rslink.getString("tag").equals("h3"))
								pos.h3Pos.addElement(rslink.getInt("position"));
							else if (rslink.getString("tag").equals("h4"))
								pos.h4Pos.addElement(rslink.getInt("position"));
							else if (rslink.getString("tag").equals("h5"))
								pos.h5Pos.addElement(rslink.getInt("position"));
							else if (rslink.getString("tag").equals("h6"))
								pos.h6Pos.addElement(rslink.getInt("position"));
							
						}
						wp.addElement(pos);
						linksID.addElement(v);
						
						positions.addElement(wp);
					}
					
				}
				Vector<urls> vu = new Vector<urls>();
				for(int j=0; j<linksID.elementAt(0).size(); j++){
				
					Vector<Integer> idx = new Vector<Integer>();
					idx.addElement(j);
					boolean flag = false;
					for(int i=1; i<linksID.size(); i++){
						int x = linksID.elementAt(i).indexOf(linksID.elementAt(0).elementAt(j));
						
						if (x == -1){
							flag = true;
							break;
						}
						idx.addElement(x);
					}
					if (!flag){
						urls U = new urls();
						for(int q = 0; q< positions.elementAt(0).elementAt(idx.elementAt(0)).bodyPos.size(); q++){	
							
							boolean check = false;
							for(int w = 1; w<positions.size(); w++){
								
								if(Collections.binarySearch(positions.elementAt(w).elementAt(idx.elementAt(w)).bodyPos, positions.elementAt(0).elementAt(idx.elementAt(0)).bodyPos.elementAt(q)+1) < 0)
								{
									check = true;
									break;
								}
							}
							if (!check)
							{
								U.urlID = positions.elementAt(0).elementAt(idx.elementAt(0)).urlID;
								U.wordsInFile = positions.elementAt(0).elementAt(idx.elementAt(0)).wordsInFile;
								U.bodyPos.addElement(positions.elementAt(0).elementAt(idx.elementAt(0)).bodyPos.elementAt(q));
							}
						}
						for(int q = 0; q< positions.elementAt(0).elementAt(idx.elementAt(0)).titlePos.size(); q++){	
							boolean check = false;
							for(int w = 1; w<positions.size(); w++){
								
								if(Collections.binarySearch(positions.elementAt(w).elementAt(idx.elementAt(w)).titlePos, positions.elementAt(0).elementAt(idx.elementAt(0)).titlePos.elementAt(q)+1) < 0)
								{
									check = true;
									break;
								}
							}
							if (!check)
							{
								U.urlID = positions.elementAt(0).elementAt(idx.elementAt(0)).urlID;
								U.wordsInFile = positions.elementAt(0).elementAt(idx.elementAt(0)).wordsInFile;
								U.titlePos.addElement(positions.elementAt(0).elementAt(idx.elementAt(0)).titlePos.elementAt(q));
							}
						}
						for(int q = 0; q< positions.elementAt(0).elementAt(idx.elementAt(0)).h1Pos.size(); q++){	
							boolean check = false;
							for(int w = 1; w<positions.size(); w++){
								if(Collections.binarySearch(positions.elementAt(w).elementAt(idx.elementAt(w)).h1Pos, positions.elementAt(0).elementAt(idx.elementAt(0)).h1Pos.elementAt(q)+1) < 0)
								{
									check = true;
									break;
								}
							}
							if (!check)
							{
								U.urlID = positions.elementAt(0).elementAt(idx.elementAt(0)).urlID;
								U.wordsInFile = positions.elementAt(0).elementAt(idx.elementAt(0)).wordsInFile;
								U.h1Pos.addElement(positions.elementAt(0).elementAt(idx.elementAt(0)).h1Pos.elementAt(q));
							}
						}
						for(int q = 0; q< positions.elementAt(0).elementAt(idx.elementAt(0)).h2Pos.size(); q++){	
							boolean check = false;
							for(int w = 1; w<positions.size(); w++){
								System.out.println(positions.elementAt(w).elementAt(idx.elementAt(w)).h2Pos);
								System.out.println(positions.elementAt(0).elementAt(idx.elementAt(0)).h2Pos.elementAt(q)+1);
								System.out.println(Collections.binarySearch(positions.elementAt(w).elementAt(idx.elementAt(w)).h2Pos, positions.elementAt(0).elementAt(idx.elementAt(0)).h2Pos.elementAt(q)+1));
								if(Collections.binarySearch(positions.elementAt(w).elementAt(idx.elementAt(w)).h2Pos, positions.elementAt(0).elementAt(idx.elementAt(0)).h2Pos.elementAt(q)+1) < 0)
								{
									check = true;
									break;
								}
							}
							if (!check)
							{
								U.urlID = positions.elementAt(0).elementAt(idx.elementAt(0)).urlID;
								U.wordsInFile = positions.elementAt(0).elementAt(idx.elementAt(0)).wordsInFile;
								U.h2Pos.addElement(positions.elementAt(0).elementAt(idx.elementAt(0)).h2Pos.elementAt(q));
							}
						}
						for(int q = 0; q< positions.elementAt(0).elementAt(idx.elementAt(0)).h3Pos.size(); q++){	
							boolean check = false;
							for(int w = 1; w<positions.size(); w++){
								
								if(Collections.binarySearch(positions.elementAt(w).elementAt(idx.elementAt(w)).h3Pos, positions.elementAt(0).elementAt(idx.elementAt(0)).h3Pos.elementAt(q)+1) < 0)
								{
									check = true;
									break;
								}
							}
							if (!check)
							{
								U.urlID = positions.elementAt(0).elementAt(idx.elementAt(0)).urlID;
								U.wordsInFile = positions.elementAt(0).elementAt(idx.elementAt(0)).wordsInFile;
								U.h3Pos.addElement(positions.elementAt(0).elementAt(idx.elementAt(0)).h3Pos.elementAt(q));
							}
						}
						for(int q = 0; q< positions.elementAt(0).elementAt(idx.elementAt(0)).h4Pos.size(); q++){	
							boolean check = false;
							for(int w = 1; w<positions.size(); w++){
								
								if(Collections.binarySearch(positions.elementAt(w).elementAt(idx.elementAt(w)).h4Pos, positions.elementAt(0).elementAt(idx.elementAt(0)).h4Pos.elementAt(q)+1) < 0)
								{
									check = true;
									break;
								}
							}
							if (!check)
							{
								U.urlID = positions.elementAt(0).elementAt(idx.elementAt(0)).urlID;
								U.wordsInFile = positions.elementAt(0).elementAt(idx.elementAt(0)).wordsInFile;
								U.h4Pos.addElement(positions.elementAt(0).elementAt(idx.elementAt(0)).h4Pos.elementAt(q));
							}
						}
						for(int q = 0; q< positions.elementAt(0).elementAt(idx.elementAt(0)).h5Pos.size(); q++){	
							boolean check = false;
							for(int w = 1; w<positions.size(); w++){
								
								if(Collections.binarySearch(positions.elementAt(w).elementAt(idx.elementAt(w)).h5Pos, positions.elementAt(0).elementAt(idx.elementAt(0)).h5Pos.elementAt(q)+1) < 0)
								{
									check = true;
									break;
								}
							}
							if (!check)
							{
								U.urlID = positions.elementAt(0).elementAt(idx.elementAt(0)).urlID;
								U.wordsInFile = positions.elementAt(0).elementAt(idx.elementAt(0)).wordsInFile;
								U.h5Pos.addElement(positions.elementAt(0).elementAt(idx.elementAt(0)).h5Pos.elementAt(q));
							}
						}
						for(int q = 0; q< positions.elementAt(0).elementAt(idx.elementAt(0)).h6Pos.size(); q++){	
							boolean check = false;
							for(int w = 1; w<positions.size(); w++){
								
								if(Collections.binarySearch(positions.elementAt(w).elementAt(idx.elementAt(w)).h6Pos, positions.elementAt(0).elementAt(idx.elementAt(0)).h6Pos.elementAt(q)+1) < 0)
								{
									check = true;
									break;
								}
							}
							if (!check)
							{
								U.urlID = positions.elementAt(0).elementAt(idx.elementAt(0)).urlID;
								U.wordsInFile = positions.elementAt(0).elementAt(idx.elementAt(0)).wordsInFile;
								U.h6Pos.addElement(positions.elementAt(0).elementAt(idx.elementAt(0)).h6Pos.elementAt(q));	
							}
						}
						if(U.bodyPos.size() !=0 || U.titlePos.size() !=0 ||U.h2Pos.size() !=0||U.h1Pos.size() !=0||U.h3Pos.size() !=0||U.h4Pos.size() !=0||U.h5Pos.size() !=0||U.h6Pos.size() !=0){
							vu.addElement(U);
						}
					}
				}
				returnVec.addElement(vu);
				linksID = new Vector<Vector<Integer>>();
				for (int i=0; i<words.length; i++){
					//stemming
					System.out.println(stem.AccessStemmer(words[i]));
					System.out.println(words[i]);
					if(!binarysearch(words[i], arr) && words[i] != stem.AccessStemmer(words[i])){
						String stemmedWord = stem.AccessStemmer(words[i]);
						Vector <Integer> v = new Vector<Integer>();
						urls pos = new urls();
						Vector <urls> wp = new Vector<urls>(); 
						String sql = "SELECT url, position, tag, l.kLid from keywordlink l, keywordposition p  where keyword = '" + stemmedWord + "' and  l.kLid = p.kLid";
						rslink = stmt.executeQuery(sql);
						
						int prev = -1;
						
						while (rslink.next()){
							if(rslink.getInt("kLid") != prev ){
								if(pos.bodyPos.size() !=0 || pos.titlePos.size() !=0 ||pos.h2Pos.size() !=0||pos.h1Pos.size() !=0||pos.h3Pos.size() !=0||pos.h4Pos.size() !=0||pos.h5Pos.size() !=0||pos.h6Pos.size() !=0)
								{
									wp.addElement(pos);
									pos = new urls();
								}
								prev = rslink.getInt("kLid");
								pos.urlID = getUrlID(rslink.getString("url"));
								String wordsQuery = "SELECT wordsCount from wordsinlink where url = '"+ rslink.getString("url") +"'";
								ResultSet result = stmt3.executeQuery(wordsQuery);
								while(result.next())
									pos.wordsInFile = result.getInt("wordsCount");
								v.addElement(getUrlID(rslink.getString("url")));
								
							}
							if (rslink.getString("tag").equals("Body"))
								pos.bodyPos.addElement(rslink.getInt("position")); 
							else if (rslink.getString("tag").equals("Title"))
								pos.titlePos.addElement(rslink.getInt("position"));
							else if (rslink.getString("tag").equals("h1"))
								pos.h1Pos.addElement(rslink.getInt("position"));
							else if (rslink.getString("tag").equals("h2"))
								pos.h2Pos.addElement(rslink.getInt("position"));
							else if (rslink.getString("tag").equals("h3"))
								pos.h3Pos.addElement(rslink.getInt("position"));
							else if (rslink.getString("tag").equals("h4"))
								pos.h4Pos.addElement(rslink.getInt("position"));
							else if (rslink.getString("tag").equals("h5"))
								pos.h5Pos.addElement(rslink.getInt("position"));
							else if (rslink.getString("tag").equals("h6"))
								pos.h6Pos.addElement(rslink.getInt("position"));
						}
						wp.addElement(pos);
						linksID.addElement(v);
						stemmedVec.addElement(wp);
					}
				}
			}
			//catch (SQLException e) 
			catch (Exception e)
			{
				System.out.println("error executing query " + e);
			}
		}
				
				  Vector<Integer> linksIDs = new Vector<Integer>();
				  Vector<displayLink> display = new Vector<displayLink> ();
				  Ranker R = new Ranker();
				  linksIDs = R.combinedFinalRanking(positions,stemmedVec,returnVec); // where positions is array of all words in query  returnVec the whole phrase and stemmedVec
				  
				  for(int i=0; i<linksIDs.size(); i++)
				  {
				  		display.addElement(linkData(linksIDs.elementAt(i)));
				  }
				  
				  return display;
		/*
		// test
		Vector<displayLink> display = new Vector<displayLink> ();
		display.addElement(linkData(208953));
				  return display;
		*/		 				
	}
}
