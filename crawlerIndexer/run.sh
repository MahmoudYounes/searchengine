#!/bin/bash
# updated bash file to run SearchEngine generic
# this bash file describes how to run and required files for running
# please chmod the code files to be readable and writtable and give enough permissions for the script to execute.

# getting directory to open
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

#removing any .class file 
for file in $DIR/src/* ; do
    if [ ${file: -6} == ".class" ]
    then
    	rm $file
    fi
done;

#preparing libs
LIBPATH=$DIR/lib
JARS=$LIBPATH/jsoup-1.8.3.jar:$LIBPATH/jrobotx-0.2.jar:$LIBPATH/apache-logging-log4j.jar:$LIBPATH/org.apache.commons.io.jar:$LIBPATH/mysql-connector-java-5.1.38-bin.jar

echo "opening directory"
cd "$DIR/src"

echo "compiling"
javac -cp $JARS *.java

echo "running"
java -cp .:$JARS SearchEngine
