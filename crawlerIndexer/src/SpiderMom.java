import java.lang.*;
import java.sql.*;
import java.util.*;
import java.lang.*;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import java.io.*;
import java.net.*;

import com.trigonic.jrobotx.*;


public class SpiderMom implements Runnable
{	
	private static SpiderMom staticMom = null;
	private int babySpidersCount = 0;	
	MySqlDatabase hive;									//hive: the nest of urls stored
	Queue<String> preys;								//preys: the urls we are hunting	

	public SpiderMom(int _spidersCount)
	{
		hive  = new MySqlDatabase();			
		babySpidersCount = _spidersCount;
	}

	
	
	public static SpiderMom getInstance(int _c)
	{
		if (staticMom == null)
		{
			staticMom = new SpiderMom(_c);
		}
		return staticMom;
	}
	


	public synchronized void markInvalid(String url)
	{			
		hive.markInvalidURL(url);
	}

	public synchronized String getPrey()
	{
		if (preys == null || preys.isEmpty())
		{			
			prepareUrls();
		}

		if (preys == null || preys.isEmpty())
			return null;


		updateURLAsTaken(preys.peek());
		
		return preys.remove();				
		
	}

	public synchronized void insertPrey(String url)
	{
		/*
			inserting new url with the following parameters:
				-the url
				-it is not visited
				-it is valid url
				-it is not taken
				-the hash of the url
				-date and time of insertion
		*/
		String sql = "INSERT INTO WebLinks (`web-urls` , `visited` , `invalid` , `taken` , `web-urls-hashed` , `insertedAt`) ";
		sql       += "VALUES ('"+url+"', 0, 0, 0 , SHA1('"+url+"') , NOW());";
		try
		{
			//System.out.println(sql);
			hive.updateQuery(sql);
		}
		catch (Exception e)
		{
			System.out.println(sql);
			System.out.println(e.getMessage());
		}
	}

	public synchronized void updateUrlToVisited(String url)
	{
		try
		{			
			String sql  = "UPDATE WebLinks ";
			sql        += "SET `visited` = 1 ";
			sql        += "WHERE `web-urls` = '"+url+"'";
			

			hive.updateQuery(sql);			
		}
		catch(Exception e)
		{

		}
	}


	//selects urls from the hive (limited to 50)
	private void prepareUrls()
	{	
		try
		{		
			preys = hive.getUrls();								
		}
		catch(Exception e)
		{

		}
	}

	public void updateURLAsTaken(String url)
	{
		String sql  = "UPDATE WebLinks ";
	 	       sql += "SET `taken` = 1 ";
	 	       sql += "WHERE `web-urls` = '" + url +"'";

	 	hive.updateQuery(sql);
	}

	public void updateURLAsNonTaken(String url)
	{
		String sql  = "UPDATE WebLinks ";
	 	       sql += "SET `taken` = 0 ";
	 	       sql += "WHERE `web-urls` = '" + url +"'";
	 	       
	 	hive.updateQuery(sql);
	}

	public void run()
	{						
		System.out.println("entered crawler");
		Spiders e = new Spiders(this);		
		for (int i = 0; i < babySpidersCount; i++)
		{							
			Thread t = new Thread(e);
			t.start();			
		}
				
	}
	
}


class Spiders implements Runnable
{
	String url;
	
	private static SpiderMom mom;

	private RobotExclusion robotExclusion;

	public Spiders(SpiderMom _mom)
	{	
		mom = _mom;
		robotExclusion = new RobotExclusion();
	}

	public void run()
	{					
		//get page and parse and get links					
		while(true)
		{				
			//get a url from mom
			String url = mom.getPrey();

			try
			{
				if (url == null)
					Thread.sleep(500);		
			}
			catch(Exception e)
			{
				System.out.println(e.getMessage());
			}
										
			boolean condition = true;
			Document webPage = null;
			
			//debug
			System.out.println("entered spider");

			URL formatedURL = null;


			try 
			{
				//check if url is malformated. if so URL will throw an exception
				formatedURL = new URL(url);

				//check agains REP 
				if (!robotExclusion.allows(formatedURL, "Mozilla"))
				{
					throw new Exception("execluded due to REP : " + url);
				}

				//go ahead and get the document by jsoup.
				webPage = Jsoup.connect(url).get();
			}
			catch(Exception e)
			{			
				System.out.println(e.getMessage());								
				condition = false;
			}					


			//if an exception has been thrown for any reason, mark the url as non taken and invalid and continue
			if (!condition)		
			{
				mom.updateURLAsNonTaken(url);
				mom.markInvalid(url);
				continue;					
			}
			
			//get all link tags from the document	
			Elements links = webPage.getElementsByTag("a");
			
			//for debug
			System.out.print("spider : " + Thread.currentThread());
			System.out.println(" is hunting " + url);

			//for each link in the links
	        for (Element link : links)	        	
	        {
	        	//get the absolute url
	        	String linkurl = link.attr("abs:href");

	        	//check for protocol inclusion in the url
	        	if (!(linkurl.contains("https") && linkurl.contains("http")))
	        	{
	        		//if protocol not included, get the protocol from the host link
		        	String host = formatedURL.getHost();
		        	
		        	//if the host's protocol is https or http
		        	if (host.contains("https"))
		        	{
		        		linkurl = "https://" + linkurl;
		        	}
		        	else if (host.contains("http"))
		        	{
		        		linkurl = "http://" + linkurl;
		        	}
		        }
	        	
	        	if (linkurl.trim().equals("") || linkurl == null)
	        		continue;	
	        	//please mom insert this link
	        	mom.insertPrey(linkurl);            	            	            
	        } 	        
	        //update this url as visited and nontaken
	        mom.updateUrlToVisited(url);
	        mom.updateURLAsNonTaken(url);	                           
	    }
	}
}	
