import java.sql.*;
import java.util.*;

public class MySqlDatabase 
{
	// JDBC driver name and database URL
	//private static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";  
	private static final String DB_URL = "jdbc:mysql://localhost/SearchEngine";

	//  Database credentials
	/*
		note: please create a MySql database user for Search engine database and grant this user full privilages over this database
	*/
	private static final String USER = "root";
	private static final String PASS = "";

	private Connection conn = null;	
	private Statement batchStmt = null;
	public MySqlDatabase()
	{
		try
		{			
			//Register JDBC driver			
			Class.forName("com.mysql.jdbc.Driver");

			//Open a connection
			System.out.println("Connecting to crawler database...");
			conn = DriverManager.getConnection(DB_URL,USER,PASS);
			batchStmt = conn.createStatement();
		}
		catch (Exception e)
		{
			System.out.println("failed to register Database.");
			System.out.println(e.getMessage());
		}
	}
	public void markInvalidURL(String url)
	{
		String sql  = "UPDATE WebLinks ";
		sql        += "SET invalid = 1 ";
		sql 	   += "WHERE `web-urls` = '" + url + "';";
		
		updateQuery(sql);
	}

	public Queue<String> getUrls()
	{		
		try
		{		
			String sql 		= "SELECT DISTINCT `web-urls` ";
			sql 		   += "FROM WebLinks ";
			sql 		   += "WHERE visited = 0 and invalid = 0 and taken = 0 ";
			sql 		   += "LIMIT 50;";

			ResultSet rs = query(sql);
			
			Queue<String> urls = new LinkedList<String>();
			while(rs.next())
			{
				urls.add(rs.getString("web-urls"));
			} 			
			return urls;			
		}
		catch (Exception e)
		{
			return null;
		}														
									

	}

	public void updateQuery(String sql)
	{
		try
		{			
			Statement stmt = conn.createStatement();			
			stmt = conn.createStatement();						
			int rowsUpdated = stmt.executeUpdate(sql);
			/*
				should be un commented and tested for verbose output
				System.out.println(rowsUpdated + " rows count updated");			
			*/
		}
		catch(Exception e)
		{

		}
	}

	public ResultSet query(String sql)
	{
	
		try
		{
			Statement stmt = conn.createStatement();			
			stmt = conn.createStatement();						
			ResultSet rs = stmt.executeQuery(sql);	
			return rs;
		}
		catch(Exception e)
		{
			System.out.println("executing query failed");
			System.out.println(e.getMessage());
			return null;
		}
	}

	public void addBatch(String sql)
	{
		try
		{
			batchStmt.addBatch(sql);
		}
		catch(Exception e)
		{

		}
	}

	public void executeBatch()
	{
		try
		{
			batchStmt.executeBatch();			
			batchStmt.clearBatch();
		}
		catch(Exception e)
		{
			
		}
	}

	public Connection getConnection()
	{		
		return conn;
	}

	public boolean checkResults(ResultSet rs)
	{
		if (rs == null) 
			return false;
		return true;
	}

	public boolean closeConnection()
	{
		try
		{
			conn.close();
			return true;
		}
		catch(Exception e)
		{
			return false;
		}
	}
}