import org.jsoup.nodes.Document;
import java.util.*;

import java.io.*;
import java.net.*;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class Indexer implements Runnable
{
	public static boolean working = false;
	public Indexer() 
	{
		
		
	} 
	public boolean binarysearch (String word, List<String> arr){
		int l = 0, r = arr.size()-1;

		while(l <= r){
			int mid = (r-l) /2 + l;
			if(arr.get(mid).compareToIgnoreCase(word) > 0)
				r = mid-1;
			else if (arr.get(mid).compareToIgnoreCase(word) < 0)
				l = mid+1;
			else
				return true;
		}
		
		return false;
	}
	public String AccessStemmer(String x)
	{
            try
            {
                char[] w = new char[501];
	  	  Stemmer s = new Stemmer();
	 
	  	  char ch;
	  	  int f = 0;
	  	  int j = 0;
	  	  while(true)
		 {
			 ch = Character.toLowerCase(x.charAt(f));
		     w[j] = ch;
		     if (j < 500) j++;
		     f++;
		     if (f < x.length())
		    	 ch = x.charAt(f);
		     else if (f == x.length())
		     { 
		        /* to test add(char ch) */
		    	for (int c = 0; c < j; c++) s.add(w[c]);
		
		        /* or, to test add(char[] w, int j) */
		        /* s.add(w, j); */
		
		        s.stem();
		        {  String u;
		
		          /* and now, to test toString() : */
		           u = s.toString();
		
		           /* to test getResultBuffer(), getResultLength() : */
		           /* u = new String(s.getResultBuffer(), 0, s.getResultLength()); */
		
		               //System.out.print(u);
		               return u;
		             }
		         }
		     }
		      //   System.out.print(ch);
            }
            catch(Exception e)
            {
                return "";
            }
		  
	}


	public void run() 
	{
	
            try { //added
            //added : change static variable
            working = true;
            
            System.out.println("INDEXER: Started!");
                    
		//Indexer inst = new Indexer();
		IndexerDB DB = new IndexerDB();
		//DB.connectToCrawler();
		DataCell cell = new DataCell();
		String [] links = DB.getURLs();//{"http://www.google.com/search?q=lakshman"}; 
		if (links == null)
		{
			working = false;
			return;
		}

		List<String> arr = new ArrayList<String>();
		Scanner x = null;
		try {
			x = new Scanner (new File("stoppingwords.txt")); 
		}
		catch(Exception e){
			System.out.println("INDEXER: Couldn't find the 'stoppingwords.txt' file.");
		}
		while (x.hasNext())
		{
			arr.add(x.next());
		}
		x.close();
		Document doc;
                
		//try {
                
                int LinksPerThread = (int)Math.round(links.length / 8.0);
                int pr = Thread.currentThread().getPriority();
                int st = (pr - 1)*LinksPerThread;
                int end = st + LinksPerThread - 1;
                
                if(Thread.currentThread().getName() == "Thread Last")
                    end = links.length -1;
                
                        for(int i=st; i<=end; i++)
			{
                            // added
                            DB.DeIndex(links[i]);
                                                        
                            System.out.println("INDEXER Th"+pr+" : Working on url "+(i+1)+" / "+links.length+" :"+links[i]);
                                                       
                            try
                            {
                                doc = Jsoup.connect(links[i]).userAgent("Chrome").timeout(10000).get();
                                //10 seconds timeout
                            }
                            catch(SocketTimeoutException E)
                            {
                                System.out.println("INDEXER Th"+pr+" : connection timeout : "+links[i]);
                                continue;
                            }
                                Element body = doc.body();
				Element head = doc.head();
				Elements h1 = doc.select("h1");
				Elements h2 = doc.select("h2");
				Elements h3 = doc.select("h3");
				Elements h4 = doc.select("h4");
				Elements h5 = doc.select("h5");
				Elements h6 = doc.select("h6");
				String txt = body.text();
				String headtxt = head.text();
				String h1txt = h1.text();
				String h2txt = h2.text();
				String h3txt = h3.text();
				String h4txt = h4.text();
				String h5txt = h5.text();
				String h6txt = h6.text();
				String[] words = txt.replaceAll("[^a-zA-Z ]", "").trim().toLowerCase().split("\\s+");
				String[] headerwords = headtxt.replaceAll("[^a-zA-Z ]", "").trim().toLowerCase().split("\\s+");
				String[] h1words = h1txt.replaceAll("[^a-zA-Z ]", "").trim().toLowerCase().split("\\s+");
				String[] h2words = h2txt.replaceAll("[^a-zA-Z ]", "").trim().toLowerCase().split("\\s+");
				String[] h3words = h3txt.replaceAll("[^a-zA-Z ]", "").trim().toLowerCase().split("\\s+");
				String[] h4words = h4txt.replaceAll("[^a-zA-Z ]", "").trim().toLowerCase().split("\\s+");
				String[] h5words = h5txt.replaceAll("[^a-zA-Z ]", "").trim().toLowerCase().split("\\s+");
				String[] h6words = h6txt.replaceAll("[^a-zA-Z ]", "").trim().toLowerCase().split("\\s+");
				
                                DB.insertLinkResult(links[i], headtxt, txt);
                                
				String SQLquery = "SELECT * from wordsinlink where url = '"+links[i]+"'";
				//System.out.println(SQLquery);
				ResultSet rs = DB.query(SQLquery);
				try {
					if (rs.next())
					{}
					else{
						int size = words.length+ headerwords.length;
						SQLquery = "INSERT INTO wordsinlink(wordsCount,url) values (" + Integer.toString(size) + ", '"+links[i]+"')";
						DB.queryupdate(SQLquery);
					}
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				for (int j=0; j<h1words.length && h1txt.length() != 0; j++)
				{
						cell.keyword = h1words[j];
						cell.position = j;
						cell.url = links[i];
						cell.tag = "h1";
                        cell.stemmed = 0;
                        DB.insertKeyword(cell);                           
                    //added: send the stemmed version (if it's different)
                        String tmp = this.AccessStemmer(h1words[j]);
                        if(!tmp.equals(cell.keyword))
                        {
                            cell.keyword = tmp;
                            cell.stemmed = 1;
                            DB.insertKeyword(cell);
                        }
					//}
				}
				for (int j=0; j<h2words.length && h2txt.length() != 0; j++)
				{
						cell.keyword = h2words[j];
						cell.position = j;
						cell.url = links[i];
						cell.tag = "h2";
                        cell.stemmed = 0;
                        DB.insertKeyword(cell);                           
                    //added: send the stemmed version (if it's different)
                        String tmp = this.AccessStemmer(h2words[j]);
                        if(!tmp.equals(cell.keyword))
                        {
                            cell.keyword = tmp;
                            cell.stemmed = 1;
                            DB.insertKeyword(cell);
                        }
					//}
				}
				for (int j=0; j<h3words.length && h3txt.length() != 0; j++)
				{
						cell.keyword = h3words[j];
						cell.position = j;
						cell.url = links[i];
						cell.tag = "h3";
                        cell.stemmed = 0;
                        DB.insertKeyword(cell);                           
                    //added: send the stemmed version (if it's different)
                        String tmp = this.AccessStemmer(h3words[j]);
                        if(!tmp.equals(cell.keyword))
                        {
                            cell.keyword = tmp;
                            cell.stemmed = 1;
                            DB.insertKeyword(cell);
                        }
					//}
				}
				for (int j=0; j<h4words.length && h4txt.length() != 0; j++)
				{
						cell.keyword = h4words[j];
						cell.position = j;
						cell.url = links[i];
						cell.tag = "h4";
                        cell.stemmed = 0;
                        DB.insertKeyword(cell);                           
                    //added: send the stemmed version (if it's different)
                        String tmp = this.AccessStemmer(h4words[j]);
                        if(!tmp.equals(cell.keyword))
                        {
                            cell.keyword = tmp;
                            cell.stemmed = 1;
                            DB.insertKeyword(cell);
                        }
					//}
				}
				for (int j=0; j<h5words.length && h5txt.length() != 0; j++)
				{
						cell.keyword = h5words[j];
						cell.position = j;
						cell.url = links[i];
						cell.tag = "h5";
                        cell.stemmed = 0;
                        DB.insertKeyword(cell);                           
                    //added: send the stemmed version (if it's different)
                        String tmp = this.AccessStemmer(h5words[j]);
                        if(!tmp.equals(cell.keyword))
                        {
                            cell.keyword = tmp;
                            cell.stemmed = 1;
                            DB.insertKeyword(cell);
                        }
					//}
				}
				for (int j=0; j<h6words.length && h6txt.length() != 0; j++)
				{
						cell.keyword = h6words[j];
						cell.position = j;
						cell.url = links[i];
						cell.tag = "h6";
                        cell.stemmed = 0;
                        DB.insertKeyword(cell);                           
                    //added: send the stemmed version (if it's different)
                        String tmp = this.AccessStemmer(h6words[j]);
                        if(!tmp.equals(cell.keyword))
                        {
                            cell.keyword = tmp;
                            cell.stemmed = 1;
                            DB.insertKeyword(cell);
                        }
					//}
				}
				
				for (int j=0; j<headerwords.length; j++)
				{
						cell.keyword = headerwords[j];
						cell.position = j;
						cell.url = links[i];
						cell.tag = "Title";
                                                cell.stemmed = 0;
                                                DB.insertKeyword(cell);
                                                
                                            //added: send the stemmed version (if it's different)
                                                String tmp = this.AccessStemmer(headerwords[j]);
                                                if(!tmp.equals(cell.keyword))
                                                {
                                                    cell.keyword = tmp;
                                                    cell.stemmed = 1;
                                                    DB.insertKeyword(cell);
                                                }
					//}
				}
				for (int j=0; j<words.length; j++)
				{
					if(!this.binarysearch(words[j], arr))
					{
                                            /*
						cell.keyword = this.AccessStemmer(words[j]);
						cell.position = j;
						cell.url = links[i];
						cell.tag = "Body";
                                                DB.insertKeyword(cell);
                                            */
                                            
                                            //added : insert non-stemmed
						cell.keyword = words[j];
						cell.position = j;
						cell.url = links[i];
						cell.tag = "Body";
                                                cell.stemmed = 0;
                                                DB.insertKeyword(cell);
                                                
                                            //added: send the stemmed version (if it's different)
                                                String tmp = this.AccessStemmer(words[j]);
                                                if(!tmp.equals(cell.keyword))
                                                {
                                                    cell.keyword = tmp;
                                                    cell.stemmed = 1;
                                                    DB.insertKeyword(cell);
                                                }
                                                
					}
				}
                                
                               
                               
			}
                        
                         // added : close the connection
                        DB.closeConnection();
                        
                        // added : change static variable
                        working = false;
                        System.out.println("INDEXER: Finished successfully!");
			
		} catch (IOException e) {
			e.printStackTrace();
                        working = false;
                        System.out.println("INDEXER: Unexpected Stop");
		}
	}
}