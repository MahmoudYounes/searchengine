import java.sql.*;

/*
 * @author Ahmed Mustafa
 */

public class IndexerDB
{
    // JDBC driver name and database URL
    private static final String DB_URL = "jdbc:mysql://localhost/indexerdb";
    private static final String USER = "root";
    private static final String PASS = "pD123456";
    
    // Crawler's database data
    private static final String CDB_URL = "jdbc:mysql://localhost/SearchEngine";
    private static final String CUSER = "root";
    private static final String CPASS = "pD123456";

    private Connection conn = null; 
    
    private Connection crawlerConn = null;    

    public IndexerDB()
    {
        try
        {           
            //Register JDBC driver          
            Class.forName("com.mysql.jdbc.Driver");

            //Open a connection
            conn = DriverManager.getConnection(DB_URL,USER,PASS);
            System.out.println("INDEXER: Database connected!");
        }
        catch (Exception e)
        {
            System.out.println("INDEXER: Error connecting to DB: "+e.getMessage());
        }
    }

    public ResultSet query(String sql)
    {
        try
        {
            Statement stmt = conn.createStatement();
            //System.out.println("Creating statement...");
            ResultSet rs = stmt.executeQuery(sql);  
            return rs;
        }
        catch(Exception e)
        {
            System.out.println("INDEXER: Error executing query: "+e.getMessage());
            return null;
        }
    }

    public Connection getConnection()
    {       
    return conn;
    }

    public boolean closeConnection()
    {
    try
        {
            conn.close();
            System.out.println("INDEXER: Database disconnected");
            return true;
    }
    catch(Exception e)
    {
            System.out.println("INDEXER: Error closing connection with DB: "+e.getMessage());
            return false;
    }
    }
    
    // funcitons added by Ahmed Mustafa
    
    public int queryupdate(String sql)
    {
        //returns number of rows affected
        try
        {
            Statement stmt = conn.createStatement();
            return stmt.executeUpdate(sql); 
        }
        catch(Exception e)
        {
            System.out.println("INDEXER: Error executing update query: "+e.getMessage());
            System.out.println("Wrong SQL :"+sql);
            return -1;
        }
    }
    
    public boolean insertLinkResult(String url, String title, String body)
    {
        String q="";
        try
        {
            q  = "DELETE FROM `linkresult` WHERE `url`=\'"+url+"\'";
            queryupdate(q);
            
            String subBody;
            
            if(body.length() <= 20)
                subBody = body;
            else
                subBody = body.substring(0,19);
            
            title = title.replaceAll("'", "\\\\'");
            subBody = subBody.replaceAll("'", "\\\\'");
            
            q  = "INSERT INTO `linkresult`(`url`, `title`, `body`) "
               + "VALUES (\'"+url+"\',\'"+title+"\',\'"+subBody+"\')";
                       
            //System.out.println(q);
            int check = queryupdate(q);
            
            if(check == -1)
                return false;
            
            return true;
        }
        catch(Exception e)
        {       
            System.out.println("INDEXER: Error inserting in DB (linkresult): "+e.getMessage());
            System.out.println(q);
            return false;
        }
    }
    
    public boolean insertKeyword(DataCell dc)
    {
        try
        {
            // init. query data
            int f_title = 0;
            int f_head = 0;
            int f_body = 0;
                
            if(dc.tag=="Body")
                f_body++;
            else if(dc.tag=="Header")
                f_head++;
            else
                f_title++;
            
            String q;
            int KLid,check;
          
            //Check if (keyword & url) existed
            
            q  = "SELECT * FROM keywordlink "
               + "WHERE keyword='"+dc.keyword+"' "
               + "AND url='"+dc.url+"'";
                        
            ResultSet rs = query(q);
                      
            if (rs.next()) // not empty set : existed -> update
            {
                rs.first(); //set to first row
                
                //KLid , keyword , url , frq_title , frq_head , frq_body , frq_all
                
                // get KLid
                KLid = rs.getInt(1);
                
                /*
                // check if this input is already in the DB (keywordposition)
                q  = "SELECT * FROM keywordposition "
                   + "WHERE KLid="+Integer.toString(KLid)+" "
                   + "AND tag='"+dc.tag+"' "
                   + "AND position="+Integer.toString(dc.position);
                
                ResultSet ch = query(q);
                
                if(ch.next()) //not empty - repeated input
                {
                    System.out.println("Repeated Input");
                    return false;
                }
                */
                
                // insert in "keywordposition" table
                // if repeated input, it must throw a "primary_key" exception and return -1
                q = "INSERT INTO keywordposition "
                  + "(KLid, tag, position) "
                  + "VALUES ( "
                  + Integer.toString(KLid) + " , "
                  + "'" + dc.tag +"' , "
                  + Integer.toString(dc.position)
                  + " )";

                check = queryupdate(q);

                if( check == -1 ) //fail
                    return false;
                                
                // update frequency
                f_title += rs.getInt(4);
                f_head += rs.getInt(5);
                f_body += rs.getInt(6);
                
                q = "UPDATE keywordlink SET "
                  + "frq_title=" + Integer.toString(f_title) + " , "
                  + "frq_head=" + Integer.toString(f_head) + " , "
                  + "frq_body=" + Integer.toString(f_body) + " , "
                  + "frq_all=" + Integer.toString(f_title+f_head+f_body) + " "
                  + "WHERE KLid=" + Integer.toString(KLid);
                  
                check = queryupdate(q);
                
                if( check == -1 ) //fail
                    return false;
                
            }
            else // empty set : new -> insert
            {
                // insert in "keywordlink" table
                q  = "INSERT INTO keywordlink "
                   + "(keyword , url , frq_title , frq_head , frq_body , frq_all , stemmed) "
                   + "VALUES ( "
                   + "'"+dc.keyword+"' , "
                   + "'"+dc.url+"' , "
                   + Integer.toString(f_title) + " , "
                   + Integer.toString(f_head)+" , "
                   + Integer.toString(f_body)+" , "
                   + "1 , "
                   + Integer.toString(dc.stemmed)
                   + " )";
                
                check = queryupdate(q);
                
                if( check == -1 ) //fail
                    return false;
                
                // get KLid from "keywordlink" table
                q  = "SELECT KLid FROM keywordlink"
                   + " WHERE keyword='"+dc.keyword+"'"
                   + " AND url='"+dc.url+"'";
                        
                rs = query(q);
                rs.first();
                KLid = rs.getInt(1);
                
                // insert in "keywordposition" table
                q = "INSERT INTO keywordposition "
                  + "(KLid, tag, position) "
                  + "VALUES ( "
                  + Integer.toString(KLid) + " , "
                  + "'" + dc.tag +"' , "
                  + Integer.toString(dc.position)
                  + " )";

                check = queryupdate(q);

                if( check == -1 ) //fail
                    return false;
            }
                       
            return true;
        }
        catch(Exception e)
        {
            System.out.println("INDEXER: Error inserting in DB: "+e.getMessage());
            return false;
        }
          
    }
    
    // returns the URLs that have a specific keyword
    public String[] getURLs(String keyword)
    {
        try
        {
            String q;
        
            q = "SELECT url FROM keywordlink "
              + "WHERE keyword='" + keyword + "'";

            ResultSet rs = query(q);

            // check if empty
            if(!rs.next())
                return null;
            
            // get number of rows
            rs.last();
            int size = rs.getRow();
            
            String[] urls = new String[size];
            
            // reset cursor to first row      
            rs.first();
            
            // read URLs into strings
            for(int i=0; i<size; i++)
            {
                urls[i] = rs.getString(1);
                rs.next();
            }
                        
            return urls;
        }
        catch(Exception e)
        {
            System.out.println("INDEXER: Error getting URLs: "+e.getMessage());
            return null;
        }

    }
    
    public String[] getKeywords(String url)
    {
        try
        {
            String q;
        
            q = "SELECT keyword FROM keywordlink "
              + "WHERE url='" + url + "'";

            ResultSet rs = query(q);

            // check if empty
            if(!rs.next())
                return null;
            
            // get number of rows
            rs.last();
            int size = rs.getRow();
            
            String[] keywords = new String[size];
            
            // reset cursor to first row      
            rs.first();
            
            // read URLs into strings
            for(int i=0; i<size; i++)
            {
                keywords[i] = rs.getString(1);
                rs.next();
            }
                        
            return keywords;
        }
        catch(Exception e)
        {
            System.out.println("INDEXER: Error getting keywords: "+e.getMessage());
            return null;
        }

    }

//    public void connectToCrawler()
//    {
//        try 
//        {
//            /*
//            // connect to Crawler's database
//            String CDB_URL = "jdbc:mysql://localhost/SearchEngine";
//            String CUSER = "root";
//            String CPASS = "pD123456";
//            */
//
//            crawlerConn = DriverManager.getConnection(CDB_URL,CUSER,CPASS);
//            //Statement CStmt = conn.createStatement();        
//            System.out.println("Crawler's database connected!");
//        }
//        catch (Exception e)
//        {
//            System.out.println(e.getMessage());
//        }
//        
//    }
    
    // returns all the URLs from the Crawler's DB
    public String[] getURLs()
    {
        try
        {
            crawlerConn = DriverManager.getConnection(CDB_URL,CUSER,CPASS);
            System.out.println("INDEXER: Crawler's database connected!");

            Statement CStmt;
            // get all valid URLs
            String q;
            q = "SELECT DISTINCT `web-urls` FROM WebLinks WHERE visited = 1 and invalid=0 and taken = 0";

            // excute query
            CStmt = crawlerConn.createStatement();
            ResultSet rs = CStmt.executeQuery(q);   
            
            // check if empty
            if(!rs.next())
                return null;
            
            // get number of rows
            rs.last();
            int size = rs.getRow();
            
            String[] urls = new String[size];
            
            // reset cursor to first row      
            rs.first();
            
            // read URLs into strings
            for(int i=0; i<size; i++)
            {
                urls[i] = rs.getString(1);
                rs.next();
            }
                        
            
            // close connection
            crawlerConn.close();
            System.out.println("INDEXER: Crawler's database disconnected");
            
            return urls;
        }
        catch(Exception e)
        {
            System.out.println("INDEXER: Error getting crawler's URLs: "+e.getMessage());
            return null;
        }
    }
    
    public boolean DeIndex(String url)
    {
        // before indexing a website, remove its data to keep the updated ver.
        try 
        {
            String q;
            
            q = "DELETE FROM keywordlink WHERE url='"+url+"'";
            
            //System.out.println(q);
            
            int check = queryupdate(q);
            
            if(check == -1 || check == 0)
                return false;
            
            return true;
        }
        catch (Exception e)
        {
            System.out.println("INDEXER: Error deindexing URLs: "+e.getMessage());
            return false;
        }
    }
}
