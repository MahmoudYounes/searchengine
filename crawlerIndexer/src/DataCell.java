
/*
 * @author Ahmed Mustafa
 */
public class DataCell
{
    public String keyword;
    public String url;
    public String tag;
    public int position;
    public int stemmed;
}
