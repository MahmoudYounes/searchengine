
import java.net.*;
import java.io.*;
import java.util.*;

public class RobotEP
{
	public static boolean testURL(String _url)
	{
		/*
			return true if the url is invalid according to REP
			otherwise return false
		
		*/

			
		try
		{			
			URL url = new URL(_url);		
			String host = url.getHost();
			URL robotURL = new URL("https://" + host + "/robots.txt");
			System.out.println(robotURL.toString());			
			Scanner sc = new Scanner(robotURL.openStream());
			String begin, link;
			while(sc.hasNext())
			{
				begin = sc.next();
				if (begin.equals("Disallow:"))
				{	
					link = sc.next();
					if (_url.contains(link)) 
						return true;

				}
			}
			return false;
		}
		catch(Exception e)
		{			
			System.out.println(e.getMessage());
			return false;
		}
	}

	// public static void main(String [] args)
	// {
	// 	System.out.println(testURL("https://www.google.com/groups"));
	// }
}

