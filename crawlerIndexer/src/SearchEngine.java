// import java.util.Scanner;

public class SearchEngine
{
	public static void main(String [] args)
	{
            Indexer idx = new Indexer();
            int n = 8; //number of processes

            /*            
            Scanner in = new Scanner(System.in);
            n = in.nextInt();
            */
            
            Thread[] threads = new Thread[n];
            
            for(int i=0; i<n-1; i++)
            {
                threads[i] = new Thread(idx);
                threads[i].setPriority(i+1);
                threads[i].setName("Thread "+(i+1));
                threads[i].start();
            }
            
            // last thread
            threads[n-1] = new Thread(idx);
            threads[n-1].setPriority(n);
            threads[n-1].setName("Thread Last");
            threads[n-1].start();
            
            for(int i=0; i<n; i++)
            {
                try
                {
                    threads[i].join();
                }
                catch(Exception e)
                {
                    e.printStackTrace();
                }
            }
	}
}