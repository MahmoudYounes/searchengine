-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 19, 2016 at 10:20 PM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `indexerdb`
--
CREATE DATABASE IF NOT EXISTS `indexerdb` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `indexerdb`;

-- --------------------------------------------------------

--
-- Table structure for table `keywordlink`
--

CREATE TABLE IF NOT EXISTS `keywordlink` (
  `KLid` int(11) NOT NULL AUTO_INCREMENT,
  `keyword` text NOT NULL,
  `url` text NOT NULL,
  `frq_title` int(11) NOT NULL,
  `frq_head` int(11) NOT NULL,
  `frq_body` int(11) NOT NULL,
  `frq_all` int(11) NOT NULL,
  `stemmed` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`KLid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=97019 ;

-- --------------------------------------------------------

--
-- Table structure for table `keywordposition`
--

CREATE TABLE IF NOT EXISTS `keywordposition` (
  `KLid` int(11) NOT NULL,
  `tag` varchar(20) NOT NULL,
  `position` int(11) NOT NULL,
  PRIMARY KEY (`KLid`,`tag`,`position`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `linkresult`
--

CREATE TABLE IF NOT EXISTS `linkresult` (
  `url` text NOT NULL,
  `title` text NOT NULL,
  `body` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `wordsinlink`
--

CREATE TABLE IF NOT EXISTS `wordsinlink` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wordsCount` int(11) NOT NULL,
  `url` varchar(500) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=246 ;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `keywordposition`
--
ALTER TABLE `keywordposition`
  ADD CONSTRAINT `fk_KLid` FOREIGN KEY (`KLid`) REFERENCES `keywordlink` (`KLid`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
